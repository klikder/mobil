<?php

namespace App\Http\Controllers\Admin\Contact;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'no_hp' => 'required',
            'desc' => 'desc'
        ]);
        $attributes = [
            'name' => $request->name,
            'no_hp' => $request->no_hp,
            'desc' => $request->desc
        ];
        $contacts = Contact::create($attributes);
        return view('admin.contact.index', compact('contacts'));
    }
}
