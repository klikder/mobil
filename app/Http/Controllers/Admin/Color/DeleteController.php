<?php

namespace App\Http\Controllers\Admin\Color;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Color;

class DeleteController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Color $color)
    {
        $color->delete();
        return back()->with('success','Data berhasil didelete..!!');
    }
}
