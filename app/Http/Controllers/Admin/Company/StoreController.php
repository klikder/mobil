<?php

namespace App\Http\Controllers\Admin\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'isi' => 'required',
            'image' => 'required'
        ]);
        $attributes = [
            'title' => $request->title,
            'isi' => $request->isi,
        ];
        if ($request->file('image')) {
            $uploadedFile = $request->file('image');
            $path = $uploadedFile->store('public/company');
            $attributes = array_add($attributes, 'image', $path);
            }
        $companies = Company::create($attributes);
        return redirect()->back()->with('success','Data berhasil ditambahkan..!!');
    }
}
