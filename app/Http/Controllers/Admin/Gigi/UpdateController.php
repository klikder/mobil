<?php

namespace App\Http\Controllers\Admin\Gigi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gigi;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Gigi $gigi)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $attributes = [
            'name' => $request->name
        ];
        $gigi->update($attributes);
        return back()->with('success','Data berhasil diupdate..!!');
    }
}
