<?php

namespace App\Http\Controllers\Member\OurCar;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Car;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $ourcars = Car::orderBy('id')->get();
        return view('member.ourcar.index', compact('ourcars'));
    }
}
