<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts/admin');
});
// Member
    Route::get('/','Member\Dashboard\IndexController');
    Route::get('/abouts','Member\About\IndexController');
    Route::get('/ourcars','Member\OurCar\IndexController');
    // Route::get('/ourcar_details','Member\OurCar\DetailController@index');
    Route::get('/ourcar_details/{id}','Member\OurCar\DetailController@detail')->name('ourcar_details');
    Route::get('/pesans','Member\Contact\IndexController');
    Route::post('pesans','Member\Contact\StoreController');


// // Admin
Auth::routes();
Route::group(['middleware' => 'auth', 'role_id:1'], function () {

    Route::get('dashboard','Admin\Dashboard\IndexController');
    // User
    Route::get('users', 'Admin\Users\IndexController');
    Route::post('users', 'Admin\Users\StoreController');
    Route::put('users/{user}', 'Admin\Users\UpdateController');
    Route::delete('users/{user}', 'Admin\Users\DeleteController');

    // Gigi
    Route::get('gigis','Admin\Gigi\IndexController');
    Route::post('gigis','Admin\Gigi\StoreController');
    Route::put('gigis/{gigi}','Admin\Gigi\UpdateController');
    Route::delete('gigis/{gigi}','Admin\Gigi\DeleteController');

    // Type
    Route::get('types','Admin\Type\IndexController');
    Route::post('types','Admin\Type\StoreController');
    Route::put('types/{type}','Admin\Type\UpdateController');
    Route::delete('types/{type}','Admin\Type\DeleteController');

    // Color
    Route::get('colors','Admin\Color\IndexController');
    Route::post('colors','Admin\Color\StoreController');
    Route::put('colors/{color}','Admin\Color\UpdateController');
    Route::delete('colors/{color}','Admin\Color\DeleteController');

    // Company
    Route::get('companies','Admin\Company\IndexController');
    Route::post('companies','Admin\Company\StoreController');
    Route::put('companies/{companie}','Admin\Company\UpdateController');
    Route::delete('companies/{companie}','Admin\Company\DeleteController');

    // Car
    Route::get('cars','Admin\Car\IndexController');
    Route::post('cars','Admin\Car\StoreController');
    Route::put('cars/{car}','Admin\Car\UpdateController');
    Route::delete('cars/{car}','Admin\Car\DeleteController');

    // Kategori
    Route::get('kategoris','Admin\Kategori\IndexController');
    Route::post('kategoris','Admin\Kategori\StoreController');
    Route::put('kategoris/{kategori}','Admin\Kategori\UpdateController');
    Route::delete('kategoris/{kategori}','Admin\Kategori\DeleteController');

    // Contact
    Route::get('contacts','Admin\Contact\IndexController');
    Route::post('contacts','Admin\Contact\StoreController');
    Route::delete('contacts/{contact}','Admin\Contact\DeleteController');

});