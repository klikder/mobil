<?php

namespace App\Http\Controllers\Member\OurCar;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Car;

class DetailController extends Controller
{
    // public function index()
    // {
    //     $ourcars_details = Car::get();
    //     return view('member.ourcar.detail', compact('ourcar_details'));
    // }

    public function detail($id)
    {
        $ourcar_details = Car::findOrFail($id);
        return view('member.ourcar.detail', compact('ourcar_details'));
    }

}
