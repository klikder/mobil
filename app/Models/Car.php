<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
   protected $guarded=[];

   public function Typess()
   {
       return $this->belongsTo('App\Models\Type','name');
   }

   public function Gigiss()
   {
       return $this->belongsTo('App\Models\Gigi','name');
   }

   public function Colorss()
   {
       return $this->belongsTo('App\Models\Color','name');
   }

   public function Kategoriss()
   {
       return $this->belongsTo('App\Models\Kategori','name');
   }
}
