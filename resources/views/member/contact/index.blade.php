@extends('layouts.layout')
@section('title','Contact')
@section('content') 


 <section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('images/car-4.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-start">
        <div class="col-md-9 ftco-animate pb-5">
            <p class="breadcrumbs"><span class="mr-2"><a href="/">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>Contact <i class="ion-ios-arrow-forward"></i></span></p>
          <h1 class="mb-3 bread">Contact us</h1>
        </div>
      </div>
    </div>
  </section>

      <section class="ftco-section contact-section">
    <div class="container">
      <div class="row d-flex mb-5 contact-info justify-content-center">
          <div class="col-md-8">
              <div class="row mb-5">
                <div class="col-md-4 text-center py-4">
                    <div class="icon">
                        <span class="icon-map-o"></span>
                    </div>
                  <p><span>Address:</span>JL . Raya Serang Talaga Cikupa Tangerang</p>
                </div>
                <div class="col-md-4 text-center border-height py-4">
                    <div class="icon">
                        <span class="icon-mobile-phone"></span>
                    </div>
                  <p><span>Phone:</span> <a href="tel://1234567920">083891785420</a></p>
                </div>
                <div class="col-md-4 text-center py-4">
                    <div class="icon">
                        <span class="icon-envelope-o"></span>
                    </div>
                  <p><span>Email:</span> <a href="mailto:info@yoursite.com">nannisaa2@gmail.com</a></p>
                </div>
              </div>
        </div>
      </div>
     
      <div class="row block-9 justify-content-center mb-5">
        <div class="col-md-8 mb-md-5">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d16121.913550657016!2d106.51373133283157!3d-6.2251379547986!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e42014d899087e7%3A0xaaa3fc5d9df19980!2sNissan%20datsun%20cikupa!5e1!3m2!1sid!2sid!4v1587283182339!5m2!1sid!2sid" width="100%"  height="250%" frameborder="0" style="border:0" allowfullscreen></iframe>                    
          </div>
      </div>
      <div class="row block-9 justify-content-center mb-5">
        <div class="col-md-8 mb-md-5">
          </div>
      </div>
      <div class="row block-9 justify-content-center mb-5">
        <div class="col-md-8 mb-md-5">
          </div>
      </div>
      <div class="row block-9 justify-content-center mb-5">
        <div class="col-md-8 mb-md-5">
            <h2 class="text-center">Jika Anda punya pertanyaan <br>jangan ragu untuk mengirimi kami pesan</h2>
        <form action="{{ url('pesans') }}" class="bg-light p-5 contact-form" method="post">
          @csrf  
            <div class="form-group">
              <input type="text" name="name" id="name" class="form-control" placeholder="Your Name" autocomplete="off">
            </div>
            <div class="form-group">
              <input type="email" name="email" id="email" class="form-control" placeholder="Your Email" autocomplete="off">
            </div>
            <div class="form-group">
              <input type="text" name="no_hp" id="no_hp"  class="form-control" placeholder="Your No Handphone" autocomplete="off">
            </div>
            <div class="form-group">
              <textarea name="desc" id="desc" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
            </div>
            <div class="form-group">
              <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
            </div>
          </form>
        
        </div>
      </div>
      
    </div>
  </section>
@endsection