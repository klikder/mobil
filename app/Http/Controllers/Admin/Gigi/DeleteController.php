<?php

namespace App\Http\Controllers\Admin\Gigi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gigi;

class DeleteController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Gigi $gigi)
    {
        $gigi->delete();
        return back()->with('success','Data berhasil dihapus..!!');
    }
}
