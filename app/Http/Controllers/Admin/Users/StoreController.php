<?php

namespace App\Http\Controllers\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Hash;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
    $this->validate($request, [
        'role_id' => 'required',
        'nama' => 'required',
        'email' => 'required',
        'image' => 'required',
        'password' => 'required'
    ]);
    $attributes = [
        'role_id' => $request->role_id,
        'nama' => $request->nama,
        'email' => $request->email,
        'password' => Hash::make($request->password)
    ];
    if ($request->file('image')) {
        $uploadedFile = $request->file('image');
        $path = $uploadedFile->store('public/user');
        $attributes = array_add($attributes, 'image', $path);
        }
    $users = User::create($attributes);
    return redirect()->back()->with('success','Data berhasil disimpan...!!!');
}
}
