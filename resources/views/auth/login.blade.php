@extends('layouts.auth')
@section('content')

<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg_2.jpg');">
			<div class="p-t-30 p-b-50">
				<span class="login100-form-title p-b-20">
					<img src="auth/images/sap.png" class="img-circle" alt="" width="100px">
				</span>
				@if ($message = Session::get('success'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<p>{{ $message }}</p>
				</div>
				@endif
				<form class="login100-form validate-form p-b-33 p-t-5" action="{{ route('login') }}" autocomplete="off" method="POST">
                        @csrf

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
                            <input class="input100" type="text" class="form-control {{ $errors->has('nama') ? ' is-invalid' : '' }} form-control-lg border-left-0" name="nama" placeholder="Username"  name="nama" value="{{ old('nama') }}">
                            @if ($errors->has('nama'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nama') }}</strong>
                            </span>
                            @endif
						<span class="focus-input100" data-placeholder="&#xe82a;"></span>
					</div>
					{{-- <div id="wrapper">
						<div class="form-group has-feedback">
							<input type="password" class="form-control" id="password" placeholder="Password">
							<i class="glyphicon glyphicon-eye-open form-control-feedback">a</i>
						</div>
						</div> --}}

					<div class="wrap-input100 validate-input" data-validate="Enter password">
                            <input class="input100" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }} form-control-lg border-left-0" type="password" name="password" id="inputPassword" placeholder="Password">
                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>

					<div class="container-login100-form-btn m-t-32">
						<button class="login100-form-btn btn-success">
							<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true">
							Login
						</span>
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	

    <div id="dropDownSelect1"></div>
    @endsection