<?php

namespace App\Http\Controllers\Admin\Kategori;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kategori;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $attributes = [
            'name' => $request->name
        ];
        $kategoris = Kategori::create($attributes);
        return redirect()->back()->with('success','Data berhasil ditambahkan..!!');
    }
}
