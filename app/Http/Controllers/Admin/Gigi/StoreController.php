<?php

namespace App\Http\Controllers\Admin\Gigi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gigi;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $attributes = [
            'name' => $request->name
        ];
        $gigis = Gigi::create($attributes);
        return redirect()->back()->with('success','Data berhasil ditambahkan..!!');
    }
}
