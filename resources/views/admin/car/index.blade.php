@extends('layouts.admin') 
@section('title','Transaksi')
@section('content')

<div class="card border">
    <div style="margin:10px;">
          <div class="text-right">
               <h4>Data Transaksi</h4>
          </div>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addKat">Add Transaksi</button>
         </div>
   
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p>{{ $message }}</p>
    </div>
    @endif
    <!-- {{-- <div class="text-right">
        <div class="row mb-3">
            <div class="col-md-3">   
                <form method="GET" class="form-search">
                    <div class="input-group">
                        <input type="text" name="search" value="{{request()->search}}" class="form-control"
                            placeholder="Search name"
                            aria-label="Search name" aria-describedby="basic-addon2" autocomplete="off"
                            autofocus="on">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary"> Search </button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div> --}} -->
    <div class="table-responsive table--no-card m-b-40">
      <table class="table table-bordered table-striped table-earning">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Foto</th>
                        <th>Warna</th>
                        <th>Gigi</th>
                        <th>Harga</th>
                        <th>Desc</th>
                        <th>No Hp</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @php $no = 1; 
                      @endphp
                      @foreach ($cars as $index =>$car)
                      <tr>
                      <td>{{ $index+1 }}</td>             
                      <td>{{ $car->name }}</td>
                      <td>{{ $car->category }}</td>
                      <td><img src="{{ Storage::url($car->image) }}" alt="" srcset="" style="width: 70px; height: 70px;"></td>
                      <td>{{ $car->color }}</td>
                      <td>{{ $car->gigi }}</td>
                      <td>Rp. {{ number_format($car->harga,0) }}</td>
                      <td>{{ $car->desc }}</td>
                      <td>{{ $car->no_hp }}</td>
                      <td>              
                        <form id="form-delete-{{ $car->id }}" action="{{ url('cars/'.$car->id) }}" method="POST">
                        @csrf @method('delete')
                          <a href="#" class="btn btn-sm btn-warning" onClick="edit('{{ $car->id }}')"  data-toggle="modal" data-target="#updateSat">Edit</a>
                          <a href="javascript:void(0)" class="btn btn-sm btn-danger"
                          onClick="confirmDelete({{ $car->id }})">Delete</a>         
                          </form>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                </table> 
              </div>
              {{-- <!-- {{ $ins->links() }} --> --}}
            </div>
          </div>        


          <!-- {{-- <div id="table-container">
              @include('member.ins.index')
          </div> --}} -->

            <div class="modal fade" id="addKat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <form action="{{ url('cars') }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Data Transaksi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <div class="form-group row">
              <label for="id_car" class="col-sm-3 col-form-label">ID</label>
                <div class="col-sm-9">
                  <input type="id_car" class="form-control" id="id_car" name="id_car" aria-describedby="id_car" placeholder="Your ID">
                </div>
              </div>
              <div class="form-group row">
                <label for="type" class="col-sm-3 col-form-label">Nama</label>
                  <div class="col-sm-9">
                    <select name="name" class="form-control">
                        <option>-- Pilih Type --</option>
                        @foreach(Helper::getTypess() as $type)
                        <option value="{{$type->name}}">{{$type->name}}</option>
                        @endforeach
                    </select>       
                  </div>
                </div>
            <div class="form-group row">
                <label for="kategori" class="col-sm-3 col-form-label">Kategori</label>
                  <div class="col-sm-9">
                    <select name="category" class="form-control">
                        <option>-- Pilih Kategori --</option>
                        @foreach(Helper::getKategoriss() as $type)
                        <option value="{{$type->name}}">{{$type->name}}</option>
                        @endforeach
                    </select>       
                  </div>
                </div>
                <div class="form-group row">
                <label for="gigi" class="col-sm-3 col-form-label">Gigi</label>
                  <div class="col-sm-9">
                    <select name="gigi" class="form-control">
                        <option>-- Pilih Gigi --</option>
                        @foreach(Helper::getGigiss() as $type)
                        <option value="{{$type->name}}">{{$type->name}}</option>
                        @endforeach
                    </select>       
                  </div>
                </div>
                <div class="form-group row">
                        <label for="image" class="col-sm-3 col-form-label">Image</label>
                            <div class="col-sm-9">
                            <input type="file" class="dropify" name="image" accept="image/*">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="color" class="col-sm-3 col-form-label">Warna</label>
                        <div class="col-sm-9">
                            <select name="color" class="form-control">
                                <option>-- Pilih Warna --</option>
                                @foreach(Helper::getColorss() as $type)
                                <option value="{{$type->color}}">{{$type->color}}</option>
                                @endforeach
                    </select>       
                  </div>
                </div>
                <div class="form-group row">
                  <label for="harga" class="col-sm-3 col-form-label">Harga</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="harga" name="harga" aria-describedby="harga" placeholder="Your Buy" onKeyUp="formatNum(this)" maxlength="12">
                    </div>
                  </div>
                  <div class="form-group row">
                        <label for="desc" class="col-sm-3 col-form-label">Description</label>
                            <div class="col-sm-9">
                            <textarea name="desc" class="form-control summernote" id="desc"  aria-describedby="username" placeholder="Your Description"></textarea>
                        </div>
                    </div>
                  <div class="form-group row">
                  <label for="no_hp" class="col-sm-3 col-form-label">No Handphone</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="no_hp" name="no_hp" aria-describedby="no_hp" placeholder="Your No Handphone">
                    </div>
                  </div>
                </div>
          
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Save</button>
                  </div>
                   </form>
                   </div>
                </div>
            </div>
                
            <div class="modal fade" id="viewKat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                 <form action="{{ url('cars') }}" method="post">
                    @csrf

                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Data Anggota</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                  
                    <div class="modal-body">
                      <div class="form-group row">
                        <label for="id_car" class="col-sm-3 col-form-label">ID</label>
                          <div class="col-sm-9">
                            <input type="id_car" class="form-control" id="id_car" name="id_car" aria-describedby="id_car" placeholder="Your ID">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="type" class="col-sm-3 col-form-label">Nama</label>
                            <div class="col-sm-9">
                              <select name="name" class="form-control">
                                  <option>-- Pilih Type --</option>
                                  @foreach(Helper::getTypess() as $type)
                                  <option value="{{$type->name}}">{{$type->name}}</option>
                                  @endforeach
                              </select>       
                            </div>
                          </div>
                      <div class="form-group row">
                          <label for="kategori" class="col-sm-3 col-form-label">Kategori</label>
                            <div class="col-sm-9">
                              <select name="name" class="form-control">
                                  <option>-- Pilih Kategori --</option>
                                  @foreach(Helper::getKategoriss() as $type)
                                  <option value="{{$type->name}}">{{$type->name}}</option>
                                  @endforeach
                              </select>       
                            </div>
                          </div>
                          <div class="form-group row">
                          <label for="gigi" class="col-sm-3 col-form-label">Gigi</label>
                            <div class="col-sm-9">
                              <select name="name" class="form-control">
                                  <option>-- Pilih Gigi --</option>
                                  @foreach(Helper::getGigiss() as $type)
                                  <option value="{{$type->name}}">{{$type->name}}</option>
                                  @endforeach
                              </select>       
                            </div>
                          </div>
                          <div class="form-group row">
                                  <label for="image" class="col-sm-3 col-form-label">Image</label>
                                      <div class="col-sm-9">
                                      <input type="file" class="dropify" name="image" accept="image/*">
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label for="color" class="col-sm-3 col-form-label">Warna</label>
                                  <div class="col-sm-9">
                                      <select name="name" class="form-control">
                                          <option>-- Pilih Warna --</option>
                                          @foreach(Helper::getColorss() as $type)
                                          <option value="{{$type->name}}">{{$type->name}}</option>
                                          @endforeach
                              </select>       
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="harga" class="col-sm-3 col-form-label">Harga</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" id="harga" name="harga" aria-describedby="harga" placeholder="Your Buy" onKeyUp="formatNum(this)" maxlength="12">
                              </div>
                            </div>
                            <div class="form-group row">
                                  <label for="desc" class="col-sm-3 col-form-label">Description</label>
                                      <div class="col-sm-9">
                                      <textarea name="desc" class="form-control summernote" id="desc"  aria-describedby="username" placeholder="Your Description"></textarea>
                                  </div>
                              </div>
                            <div class="form-group row">
                            <label for="no_hp" class="col-sm-3 col-form-label">No Handphone</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" id="no_hp" name="no_hp" aria-describedby="no_hp" placeholder="Your No Handphone">
                              </div>
                            </div>
                          </div>
                    
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-success">Save</button>
                    </div>
                   </form>
                </div>
              </div>
            </div>

            <div class="modal fade" id="updateSat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                <form id="form-update" method="post" enctype="multipart/form-data">
                    @csrf @method('put')
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Data Transaksi</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                      <div class="form-group row">
                        <label for="harga" class="col-sm-3 col-form-label">Harga</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="harga" name="harga" aria-describedby="harga" placeholder="Your Buy" onKeyUp="formatNum(this)" maxlength="12">
                          </div>
                        </div>
                    </div>        
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-success">Save</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
        @endsection
        @push('js')
    
<script>
function edit(id) {
$('#form-update').attr('action','{{ url("cars") }}'+'/'+id);
    }
    function confirmDelete(id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $('#form-delete-'+id).submit();
                swal("Poof! Your data has been deleted!", {
                icon: "success",
                });
            } else {
                swal("Your data is safe!");
            }
        });
    }
    function formatNum(obj)
        {
            var current=obj.value;
            var after=current;
            current=current.replace(/,/g,"");
            var decimalpoint=current.lastIndexOf(".");
            var n;
            var d;
            if(decimalpoint>=0)
            {
                var f=current.split(".");
                d=f[1];
                n=f[0];
            }
            else
            {
                n=current;
            }
            
            var index=parseInt((n.length-1)/3);
            if(index!=0)
            {
                var prefixIndex=n.length-index*3;
                after=n.substr(0,prefixIndex)+","+n.substr(prefixIndex,3);
                for(var i=2;i<=index;i++)
                {
                    after+=","+n.substr(prefixIndex+3*(i-1),3);
                }
            
                if(decimalpoint>=0)
                {
                    after+="."+d;
                }
            }
            obj.value=after;
        }
</script>   

@endpush
