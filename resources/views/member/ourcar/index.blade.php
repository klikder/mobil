@extends('layouts.layout')
@section('title','Car')
@section('content') 



<section class="hero-wrap hero-wrap-2 js-fullheight" style="background-image: url('images/car-12.jpg');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text js-fullheight align-items-end justify-content-start">
        <div class="col-md-9 ftco-animate pb-5">
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>About us <i class="ion-ios-arrow-forward"></i></span></p>
          <h1 class="mb-3 bread">Choose Your Car</h1>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section">
      <div class="container">
          <div class="row">
              @foreach($ourcars as $ourcar )
              <div class="col-md-3">
                  <div class="car-wrap ftco-animate">
                      <div class="img d-flex align-items-end" style="background-image: url({{ Storage::url($ourcar->image) }});">
                          <div class="price-wrap d-flex">
                          <span class="rate">Rp. {{ number_format($ourcar->harga,0) }}</span>
                              <p class="from-day">
                              <span>{{ $ourcar->color }}</span>
                              </p>
                          </div>
                      </div>
                      <div class="text p-4 text-center">
                      <h2 class="mb-0"><a href="car-single.html">{{ $ourcar->name }}</a></h2>
                      <span>Kategori: {{$ourcar->category}}</span>
                      <p class="d-flex mb-0 d-block"><a class="btn btn-black btn-outline-black mr-1">{{$ourcar->gigi}}</a> 
                        <a href="{{ route('ourcar_details',['id' => $ourcar->id])}}" class="btn btn-black btn-outline-black ml-1">Details</a></p>
                    </div>
                  </div>
              </div>
        @endforeach
      </div>
      </div>
  </section>

  @endsection