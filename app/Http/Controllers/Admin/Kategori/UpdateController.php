<?php

namespace App\Http\Controllers\Admin\Kategori;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kategori;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Kategori $kategori)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $attributes = [
            'name' => $request->name
        ];
        $kategori->update($attributes);
        return back()->with('success','Data berhasil diupdate..!!');
    }
}
