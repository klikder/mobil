<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Car;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
       $this->validate($request, [
           'id_car' => 'required',
           'name' => 'required',
           'category' => 'required',
           'color' => 'required',
           'image' => 'required',
           'gigi' => 'required',
           'harga' => 'required',
           'desc' => 'required',
           'no_hp' => 'required'

       ]);

       $attributes = [
            'id_car' => $request->id_car,
            'name' => $request->name,
            'category' => $request->category,
            'color' => $request->color,
            'gigi' => $request->gigi,
            'harga' => str_replace(",","",$request->harga),
            'desc' => $request->desc,
            'no_hp' => $request->no_hp,

       ];

       if ($request->file('image')) {
        $uploadedFile = $request->file('image');
        $path = $uploadedFile->store('public/Car');
        $attributes = array_add($attributes, 'image', $path);
        }
        
        $cars = Car::create($attributes);
        return redirect()->back()->with('success','Data anda berhasil ditambahkan..!!');
    }
}
