<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Car;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Car $car)
    {
        $this->validate($request, [

            'harga' => 'required',
        ]);
 
        $attributes = [
             'harga' => str_replace(",","",$request->harga),
        ];
 
         $car->update($attributes);
         return back()->with('success','Data anda berhasil diupdate..!!');
     }
    
}
