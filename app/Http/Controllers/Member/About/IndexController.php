<?php

namespace App\Http\Controllers\Member\About;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $abouts = Company::get();
        return view('member.about.index', compact('abouts'));
    }
}
