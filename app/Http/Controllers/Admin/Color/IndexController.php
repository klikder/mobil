<?php

namespace App\Http\Controllers\Admin\Color;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Color;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $colors = Color::all();
        return view('admin.color.index', compact('colors'));
    }
}
