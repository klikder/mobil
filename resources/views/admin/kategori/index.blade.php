@extends('layouts.admin') 
@section('title','Kategori')
@section('content')

    <h1 class="site-heading text-center text-white d-none d-lg-block">
      <span class="site-heading-upper text-primary mb-3"></span>
      <span class="site-heading-lower"></span>
    </h1>
   
    <div class="row">
        <div class="col-lg-9">
            <div class="form-group">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addKat">Add Kategori</button>
            <div class="pull-right">
                <h3>Data Kategori<h3>
                </div>
        </div>
        </div>
            <div class="table-responsive table--no-card m-b-40">
                <table class="table table-bordered table-striped table-earning">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                          @php $no = 1; 
                          @endphp
                          @foreach ($kategoris as $index =>$kategori)
                				  <tr>
                				  <td>{{ $index+1 }}</td>
                				  <td>{{ $kategori->name }}</td> 
                          <td>              
                            <form id="form-delete-" action="{{ url('kategoris/'.$kategori->id) }}" method="POST">
                              @csrf @method('delete')
                                  <a href="#" class="btn btn-warning btn-sm" onClick="edit('{{ $kategori->id }}')"  data-toggle="modal" data-target="#updateKat">Edit</a>
                                  <button class="btn btn-sm btn-danger" type="submit">Delete</button>            
                              </form>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table> 
                  </div>
                </div>
              </div>        

                <div class="modal fade" id="addKat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <form action="{{ url('kategoris') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Data Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
                <div class="modal-body">
                  <div class="form-group row">
                    <label for="name" class="col-sm-3 col-form-label">Nama</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" aria-describedby="username" placeholder="Your Name">
                        {{-- <select name="jenis_kelamin" id="jenis_kelamin" class="form-control" required>
                        <option>Pilih</option>
                        <option value="Laki-laki">Laki-laki</option>
                        <option value="Perempuan">Perempuan</option>
                        </select> --}}
                      </div>
                    </div>
                  </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save</button>
                      </div>
                       </form>
                       </div>
                    </div>
                </div>
                    
                <div class="modal fade" id="viewKat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                     <form action="{{ url('kategoris') }}" method="post">
                        @csrf

                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Data Kategori</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                         <div class="modal-body">
                          <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Nama</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name" name="name" aria-describedby="username" placeholder="Your Name">>
                                 {{-- <select name="jenis_kelamin" id="jenis_kelamin" class="form-control" required>
                                  <option>Pilih</option>
                                  <option value="Laki-laki">Laki-laki</option>
                                  <option value="Perempuan">Perempuan</option>
                              </select> --}}
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                       </form>
                    </div>
                    </div>
                </div>

                <div class="modal fade" id="updateKat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <form id="form-update" method="post">
                        @csrf @method('put')
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Kategori</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                         <div class="modal-body">
                          <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Nama</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required>
                            </div>
                          </div>
                        </div>
                          
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                @endsection
                @push('js')
            <script>
            function edit(id) {
                $('#form-update').attr('action','{{ url("kategoris") }}'+'/'+id);
            }
    </script>                
@endpush