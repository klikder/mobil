<?php

namespace App\Http\Controllers\Admin\Color;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Color;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Color $color)
    {
        $this->validate($request, [
            'color' => 'required'
        ]);
        $attributes = [
            'color' => $request->color
        ];
        $color->update($attributes);
        return back()->with('success','Data berhasil diupdate..!!');
    }
}
