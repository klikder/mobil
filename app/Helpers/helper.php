<?php
use App\Models\Type;
use App\Models\Gigi;
use App\Models\Kategori;
use App\Models\Color;


class Helper
{
 
    public static function getTypess()
    {
        $type = Type::all();
        return $type;
    }
    public static function getGigiss()
    {
        $gigi = Gigi::all();
        return $gigi;
    }    
    public static function getKategoriss()
    {
        $kategori = Kategori::all();
        return $kategori;
    }    

    public static function getColorss()
    {
        $color = Color::all();
        return $color;
    }   
    // public static function notificationCount()
    // {
    //     $registrasi = Registrasi::count();
    //     return $registrasi;
    // }

 
}