<?php

namespace App\Http\Controllers\Admin\Type;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Type;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Type $type)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $attributes = [
            'name' => $request->name
        ];
        $type->update($attributes);
        return back()->with('success','Data berhasil diupdate..!!');
    }
}
