<?php

namespace App\Http\Controllers\Member\Contact;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'no_hp' => 'required',
            'desc' => 'required'
        ]);
        $attributes = [
            'name' => $request->name,
            'email' => $request->email,
            'no_hp' => $request->no_hp,
            'desc' => $request->desc
        ];
        $pesans = Contact::create($attributes);
        return redirect()->back()->with('success','Pesan anda berhasil dikirim..!!');
    }
}
