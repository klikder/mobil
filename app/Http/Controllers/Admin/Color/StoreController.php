<?php

namespace App\Http\Controllers\Admin\Color;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Color;

class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'color' => 'required'
        ]);

        $attributes = [
            'color' => $request->color
        ];

        $colors = Color::create($attributes);
        return redirect()->back()->with('success','Data berhasil ditambhakan..!!');

        }
}
