<?php

namespace App\Http\Controllers\Admin\Gigi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gigi;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $gigis = Gigi::get();
        return view('admin.gigi.index', compact('gigis'));
    }
}
