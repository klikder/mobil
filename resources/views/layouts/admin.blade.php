<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title Page-->
    <title>@yield('title')</title>

    <!-- Fontfaces CSS-->
    <link rel="shorcut icon" href="admin/login/images/os.png">
    <link href="admin/css/font-face.css" rel="stylesheet" media="all">
    <link href="admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="admin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="admin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="admin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="adminvendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="admin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="admin/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="admin/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css') }}"> 
    <link rel="stylesheet" href="{{ asset('plugins/dropify/dist/css/dropify.min.css') }}">


    <!-- Main CSS-->
    <link href="admin/css/theme.css" rel="stylesheet" media="all">

</head>


<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="/">
                                <h4><img src="admin/images/os.png"/> Tomps Mobil</h4>
                        </a>
                        <button class="hamburger--slider btn btn-default" type="button">
                            <div class="hamburger">
                                <h4>Menu</h4>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a href="/">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="">
                              <i class="fas fa-shopping-bag"></i>Penjualan</a>
                        </li>
                        <li>
                            <a href="">
                              <i class="fas fa-shopping-bag"></i>Pembelian</a>
                        </li>
                        <li>
                            <a href="">
                             <i class="fas fa-cog"></i>Setting</a>
                        </li>
                        <li class="has-sub">
                                <a class="js-arrow" href="#">
                                    <i class="fas fa-file"></i>Laporan</a>
                                <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                    <li>
                                        <a href="">Penjualan</a>
                                        <a href="">Pembelian</a>
                                    </li>
                                    
                                </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-desktop"></i>Master Data</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{ url('types') }}">Type</a>
                                </li>
                                <li>
                                    <a href="{{ url('colors') }}">Warna</a>
                                </li>
                                <li>
                                    <a href="{{ url('gigis') }}">Gigi</a>
                                </li>
                                <li>
                                    <a href="">Suplier</a>
                                </li>                                                                                
                        </ul>
                    </li>
                    <li>
                     
                    </li>
                </div>              
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <h4><img src="admin/images/os.png"/> Tomps Mobil</h4>
                </a>
            </div>
            <div class="container">
                <div class="row">
                <div class="col-md-6"> 
                   
                      
                </div>
            </div>
        </div>
           
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="has-sub">                         
                                
                
                          
                            <a class="js-arrow" href="dashboard">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                              
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-desktop"></i>Master Data</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="{{ url('types') }}">Type</a>
                                </li>
                                <li>
                                    <a href="{{ url('colors') }}">Warna</a>
                                </li>
                                <li>
                                    <a href="{{ url('gigis') }}">Gigi</a>
                                </li>
                                <li>
                                    <a href="{{ url('kategoris') }}">Kategori</a>
                                </li>
                                <li>
                                    <a href="">User</a>
                                </li>   
                            </ul>
                        </li>
                        <li>
                        <a href="{{ url('companies') }}">
                            <i class="fas fa-shopping-bag"></i> Company</a>
                      </li>                       
                    <li>
                        <a href="{{ url('cars') }}">
                          <i class="far fa-check-square"></i> Transaksi</a>
                    </li>
                    <li>
                    <a href="{{ url('contacts')}}">
                          <i class="fas fa-envelope"></i> Message</a>
                    </li>
                        
        
                        <li>
                            <a href="{{ url('users') }}">
                                <i class="fas fa-cog"></i>Setting</a>
                        </li>
                      
                        
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"><i class="menu-icon fa fa-power-off"></i>Logout </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                    </form>
                        </li>
                      
                    <!-- {{-- </ul> --}} -->
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
       
            <!-- HEADER DESKTOP-->
         
               <!-- END HEADER DESKTOP-->
                          
            <!-- MAIN CONTENT-->
            <div class="page-container">
             <div class="main-content">
                <div class="section__content section__content--p30">
                   @yield('content')                 
                    </div>
                </div>
            <!-- END MAIN CONTENT-->

            </div>
          
    <footer class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="copyright">
                    <p>Copyright © 2020 All rights reserved.</p>
                    <td>
                        <img src="admin/icon/instagram.png" style="width:3%"></i>
                        <img src="admin/icon/facebook.png" style="width:3%"></i>
                        <img src="admin/icon/twitter.png" style="width:3%">
                        <img src="admin/icon/youtube.png" style="width:3%">
                        <img src="admin/icon/whatsapp.png" style="width:3%">
                    </td>                  
                </div>
            </div>
        </div>
    </footer>
    <!-- Jquery JS-->
    <script src="admin/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="admin/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="admin/vendor/slick/slick.min.js">
    </script>
    <script src="admin/vendor/wow/wow.min.js"></script>
    <script src="admin/vendor/animsition/animsition.min.js"></script>
    <script src="admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="admin/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="admin/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="admin/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="admin/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="admin/vendor/select2/select2.min.js"></script>
    <script src="admin/vendor/sweetalert/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="admin/js/main.js"></script>
    <script src="{{ asset('plugins/dropify/dist/js/dropify.min.js') }}"></script>
    <script>   
        $('.dropify').dropify();
            </script>   
    <script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script> 
    <script>
          $('.summernote').summernote({
                tabsize: 2,
                height: 300,
                codemirror: { 
                    theme: 'monokai'
                }
            });
            </script>
@stack('js')
</body>
</html>
<!-- end document-->
