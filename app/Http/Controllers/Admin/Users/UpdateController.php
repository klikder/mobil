<?php

namespace App\Http\Controllers\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request,User $user)
    {
        $this->validate($request, [
        
            'password' => 'required'
        ]);
        $attributes = [
          
            'password' => Hash::make($request->password)
        ];
        if ($request->file('image')) {
            $uploadedFile = $request->file('image');
            $path = $uploadedFile->store('public/user');
            $attributes = array_add($attributes, 'image', $path);
            }
        $user->update($attributes);
        return response()->json('success','Data berhasil diupdate...!!!');
    }
}
