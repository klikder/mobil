@extends('layouts.admin') 
@section('title','Gigi')
@section('content')

    <div class="form-group">
        <div style="margin:10px;">
              <div class="text-right">
                   <h4>Data Gigi</h4>
              </div>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addKat">Add Gigi</button>
        </div>
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p>{{ $message }}</p>
        </div>
        @endif
        <div class="table-responsive table--no-card m-b-40">
                        <table class="table table-bordered table-striped table-earning">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                          @php $no = 1; 
                          @endphp
                          @foreach ($gigis as $index =>$gigi)
                				  <tr>
                          <td>{{ $index+1 }}</td>
                          <td>{{ $gigi->name }}</td>          
                          <td>              
                          <form id="form-delete-" action="{{ url('gigis/'.$gigi->id) }}" method="POST">
                            @csrf @method('delete')
                            <a href="#" class="btn btn-warning btn-sm" onClick="edit('{{ $gigi->id }}')"  data-toggle="modal" data-target="#updateSat">Edit</a>
                            <button class="btn btn-sm btn-danger" type="submit">Delete</button>            
                                </form>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table> 
                  </div>
                </div>
              </div>        

                <div class="modal fade" id="addKat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <form action="{{ url('gigis') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Data Type</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
  
                <div class="modal-body">
                  <div class="form-group row">
                    <label for="name" class="col-sm-3 col-form-label">Nama</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" aria-describedby="username" placeholder="Your Name">
                      </div>
                    </div>
                    </div>
              
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save</button>
                      </div>
                       </form>
                       </div>
                    </div>
                    
                <div class="modal fade" id="viewKat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                     <form action="{{ url('gigis') }}" method="post">
                        @csrf

                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Data Type</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                      
                         <div class="modal-body">
                          <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Nama</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" id="name" name="name" aria-describedby="usernama" placeholder="Your Nama">
                            </div>
                          </div>
                          </div>
                        </div>    
                    
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                       </form>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="updateSat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <form id="form-update" method="post" enctype="multipart/form-data">
                        @csrf @method('put')
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Data Type</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        
                        <div class="modal-body">
                          <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Nama</label>
                                <div class="col-sm-9">                                                    
                                <input type="text" class="form-control" id="name" name="name" aria-describedby="usernama" >                                                  
                            </div>
                          </div>                    
                          </div>
                       
                         
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-success">Save</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
            @endsection
            @push('js')
    <script>
function edit(id) {
    $('#form-update').attr('action','{{ url("gigis") }}'+'/'+id);
        }
        function confirmDelete(id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $('#form-delete-'+id).submit();
                swal("Poof! Your data has been deleted!", {
                icon: "success",
                });
            } else {
                swal("Your data is safe!");
            }
        });
    }
    </script>    
@endpush