<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::forceCreate([
            'role_id' => 1,
            'nama' => 'adminkuy',
            'email' => 'adminkuy@gmail.com',
            'image' => 'public/user/RvGMKymr1Eb35jczb5zKDqpPM1jK6ZqjWSnpABrn.png',
            'password' => Hash::make('123'),
            'last_login' => '2020-04-22 04:11:25',
        ]);
        Role::forceCreate([
            'id' => 1,
            'role_name' => 'adminkuy',
            'created_at' => '2020-04-22 04:11:25',
            'updated_at' => '2020-04-22 04:11:25'
        ]);
    }
}
