<?php

namespace App\Http\Controllers\Admin\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company;

class UpdateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, Company $companie)
    {
        $this->validate($request, [
            'title' => 'required',
            'isi' => 'required',
            'image' => 'required'
        ]);
        $attributes = [
            'title' => $request->title,
            'isi' => $request->isi,
        ];
        if ($request->file('image')) {
            $uploadedFile = $request->file('image');
            $path = $uploadedFile->store('public/companies');
            $attributes = array_add($attributes, 'image', $path);
        }
        $companie->update($attributes);
        return back()->with('success','Data berhasil diupdate..!!');
    }
}
