<?php

namespace App\Http\Controllers\Admin\Type;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Type;


class StoreController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $attributes = [
            'name' => $request->name
        ];
        $types = Type::create($attributes);
        return redirect()->back()->with('success','Data berhasil ditambahkan..!!');
    }
}
